package com.example.home.simplecounterdown;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView countdowntext;
    private Button button;
    private CountDownTimer countDownTimer;
    private long timeLeftInmillisec = 600000; // 10 minutes
    private boolean timeRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countdowntext = findViewById(R.id.countdown_text);
        button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startstop();
            }
        });

        updateTimer();
    }

    public void startstop() {
        if (timeRunning) {
            stopTimer();
        } else {
            startTimer();
        }
    }

    public void startTimer() {
        countDownTimer = new CountDownTimer(timeLeftInmillisec, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftInmillisec = millisUntilFinished;
                updateTimer();
            }

            @Override
            public void onFinish() {

            }
        }.start();

        button.setText("PUASE");
        timeRunning = true;
    }

    public void stopTimer() {
        countDownTimer.cancel();
        button.setText("START");
        timeRunning = false;
    }

    public void updateTimer() {
        int minutes = (int) timeLeftInmillisec / 60000;
        int seconds = (int) timeLeftInmillisec % 60000 / 1000;
        String timeLeftText;

        timeLeftText = "" + minutes;
        timeLeftText += ":";
        if (seconds < 10)
            timeLeftText += "0";
            timeLeftText += seconds;
            countdowntext.setText(timeLeftText);
    }
}
